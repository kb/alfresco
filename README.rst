App to integrate with Alfresco
******************************

The Alfresco REST API documentation is here:
https://api-explorer.alfresco.com/api-explorer/

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-alfresco
  source venv-alfresco/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Release
=======

https://www.kbsoftware.co.uk/docs/
