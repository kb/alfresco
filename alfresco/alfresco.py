# -*- encoding: utf-8 -*-
import json
import logging
import os
import requests
import urllib.parse

from collections import namedtuple, OrderedDict


logger = logging.getLogger(__name__)


Node = namedtuple('Node', 'node_id name description is_site parent_id')
Site = namedtuple('Node', 'node_id name title')


class AlfrescoError(Exception):

    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr('%s, %s' % (self.__class__.__name__, self.value))


class Alfresco:

    def __init__(self, url, user, password):
        self.alfresco_url = url
        self.alfresco_pass = password
        self.alfresco_user = user

    def _http_error(self, http_verb, url, response):
        logger.error('status_code: {}'.format(response.status_code))
        logger.error('content-type: {}'.format(
            response.headers['content-type']
        ))
        try:
            logger.error('json: {}'.format(
                json.dumps(response.json(), indent=4)
            ))
        except json.decoder.JSONDecodeError:
            logger.error('data: {}'.format(response.content))
        raise AlfrescoError("Cannot {}: {}".format(http_verb, url))

    def _http_get(self, url):
        auth = (self.alfresco_user, self.alfresco_pass)
        response = requests.get(url, auth=auth)
        if response.status_code == 200: # status.HTTP_200_OK
            data = response.json()
            logger.info('json: {}'.format(json.dumps(data, indent=4)))
            return data
        else:
            self._http_error('GET', url, response)

    def _http_post(self, url, data):
        auth = (self.alfresco_user, self.alfresco_pass)
        response = requests.post(url, auth=auth, json=data)
        if response.status_code == 201: # status.HTTP_201_CREATED
            data = response.json()
            logger.info('json: {}'.format(json.dumps(data, indent=4)))
            return data
        else:
            self._http_error('POST', url, response)

    def _http_put(self, url, data):
        auth = (self.alfresco_user, self.alfresco_pass)
        response = requests.put(url, auth=auth, json=data)
        if response.status_code == 200: # status.HTTP_200_OK
            data = response.json()
            logger.info('json: {}'.format(json.dumps(data, indent=4)))
            return data
        else:
            self._http_error('PUT', url, response)

    def _node_from_data(self, data):
        entry = data['entry']
        node_type = entry['nodeType']
        properties = entry.get('properties')
        description = properties.get('cm:description') if properties else None
        return Node(
            name=entry['name'],
            node_id=entry['id'],
            description=description,
            is_site=bool(node_type == 'st:sites'),
            parent_id=entry['parentId'],
        )

    @property
    def _service_url(self):
        return self.alfresco_url

    def _site_node_id(self, node_url):
        pos = node_url.rfind('/')
        if pos > 0:
            pos = pos + 1
            node_id = node_url[pos:]
        else:
            raise AlfrescoError(
                "Cannot find node in URL: '{}'".format(node_url)
            )
        return node_id

    def _url_a(self, path=None, **kwargs):
        """Get the content URL?"""
        query_path = '/'.join([
            'alfresco',
            'api',
            '-default-',
            'public',
            'alfresco',
            'versions',
            '1',
            'nodes',
        ])
        if path:
            query_path = '/'.join([query_path, path])
        result = urllib.parse.urljoin(self._service_url, query_path)
        if kwargs:
            ordered = OrderedDict(sorted(kwargs.items()))
            result = result + '?' + urllib.parse.urlencode(ordered)
        return result

    def _url_b(self, path=None, **kwargs):
        """Get the service URL?"""
        query_path = '/'.join(['alfresco', 'service', 'api'])
        if path:
            query_path = '/'.join([query_path, path])
        result = urllib.parse.urljoin(self.alfresco_url, query_path)
        if kwargs:
            ordered = OrderedDict(sorted(kwargs.items()))
            result = result + '?' + urllib.parse.urlencode(ordered)
        return result

    def create_folder(self, alfresco_node_id, folder_name):
        url = self._url_a('{}/children'.format(alfresco_node_id))
        logger.info(url)
        data = {
            "name": folder_name,
            "title": "Anything",
            "description": "A shiny new anything",
            "nodeType": "cm:folder"
        }
        data = self._http_post(url, data)
        entry = data['entry']
        return entry['id']

    def node(self, node_id):
        query_path = '/'.join([node_id])
        url = self._url_a(query_path)
        data = self._http_get(url)
        return self._node_from_data(data)

    def node_folders(self, node_id):
        result = []
        query_path = '/'.join([node_id, 'children'])
        url = self._url_a(query_path, **{'where': '(isFolder=true)'})
        data = self._http_get(url)
        entries = data['list']['entries']
        for row in entries:
            result.append(self._node_from_data(row))
        return result

    def set_node_type(self, node_id, node_type, properties):
        """Set the node type for this piece of content.

        .. note:: The API call will throw a 400 error if the node type already
                  matches.

        """
        url = self._url_a(node_id)
        data = {
            'nodeType': node_type,
            'properties': properties,
        }
        self._http_put(url, data)

    def site_node_url(self, site_name):
        query_path = '/'.join(['sites', site_name])
        url = self._url_b(query_path)
        data = self._http_get(url)
        node_url = data['node']
        return node_url

    def sites(self):
        """Get a list of sites."""
        result = []
        url = self._url_b('sites')
        data = self._http_get(url)
        for row in data:
            node_url = row['node']
            node_id = self._site_node_id(node_url)
            result.append(Site(
                node_id=node_id,
                name=row['shortName'],
                title=row['title'],
            ))
        return result

    def upload(self, path_file, alfresco_node_id):
        """Upload to alfresco and create a 'Content' model.

        To assign the ``fn:Purchase_Invoices`` model to a document in Alfresco,
        go to the document properties and *Change Type*.

        If I change the type, the ``nodeType`` changes as follows:
        "nodeType": "fn:Purchase_Invoices",

        """
        if os.path.isfile(path_file):
            logger.info('upload: {}'.format(path_file))
            # to copy to my home folder
            # url = url_a('-my-/children')
            url = self._url_a('{}/children'.format(alfresco_node_id))
            logger.info(url)
            auth = (self.alfresco_user, self.alfresco_pass)
            with open(path_file, 'rb') as f:
                files = {'filedata': f}
                response = requests.post(url, auth=auth, files=files)
                if response.status_code == 201: # status.HTTP_201_CREATED
                    data = response.json()
                    logger.info('json: {}'.format(json.dumps(data, indent=4)))
                    node_ref = data['entry']['id']
                    logger.info('created: {}'.format(node_ref))
                    return node_ref
                else:
                    logger.error('status_code: {}'.format(response.status_code))
                    logger.error('content-type: {}'.format(
                        response.headers['content-type']
                    ))
                    try:
                        json_data = response.json()
                        logger.error(
                            'json: {}'.format(json.dumps(json_data, indent=4))
                        )
                    except json.decoder.JSONDecodeError:
                        json_data = {}
                        logger.error('data: {}'.format(response.content))
                    message = "Cannot upload file to Alfresco: '{}'".format(
                        path_file
                    )
                    error = json_data.get('error')
                    if error:
                        error_key = error.get('errorKey')
                        if error_key:
                            message = '{} ({})'.format(message, error_key)
                    raise AlfrescoError(message)
        else:
            raise AlfrescoError("Cannot find file '{}'".format(path_file))
