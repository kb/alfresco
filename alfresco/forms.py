# -*- encoding: utf-8 -*-
from django import forms

from alfresco.models import WorkflowFolder


class FolderWizardFolderForm(forms.Form):

    folder = forms.ChoiceField(choices=WorkflowFolder.objects.none())

    def __init__(self, *args, **kwargs):
        node_id = kwargs.pop('node_id')
        super().__init__(*args, **kwargs)
        f = self.fields['folder']
        f.choices = WorkflowFolder.objects.node_folders_for_choice(node_id)
        f.widget.attrs.update({'class': 'pure-u-23-24'})


class FolderWizardNodeForm(forms.Form):

    node = forms.ChoiceField(choices=WorkflowFolder.objects.none())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields['node']
        f.choices = WorkflowFolder.objects.nodes_for_choice_field()
        f.widget.attrs.update({'class': 'pure-u-23-24'})
