# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from alfresco.tasks import alfresco_copy_all


class Command(BaseCommand):

    help = "Copy documents to Alfresco"

    def handle(self, *args, **options):
        result = alfresco_copy_all()
        self.stdout.write("Complete: {}... ({})".format(self.help, result))
