# -*- encoding: utf-8 -*-
import json

from django.core.management.base import BaseCommand

from alfresco.models import Content


class Command(BaseCommand):

    help = "Test Alfresco REST API"

    #def add_arguments(self, parser):
    #    parser.add_argument('file_name')

    def _out(self, text):
        self.stdout.write(text)

    def _update_properties(self):
        content = Content.objects.last()
        if content:
            self.stdout.write("Content ID: {}".format(content.node_ref))
            json_str = content.info()
            self.stdout.write('{}'.format(json_str))
            # update the node type and properties
            properties = {
                'fn:invoiceNumber': '987654',
            }
            response = content.set_node_type('fn:Purchase_Invoices', properties)
            self.stdout.write('{}'.format(response.status_code))
            self.stdout.write('{}'.format(json.dumps(response.json(), indent=4)))
        else:
            self.stderr.write('Cannot find any content...')

    def _upload_file(self, file_name):
        Content.objects.upload(file_name)

    def _sites(self):
        result = Content.objects.sites()
        site_name = None
        for row in result:
            if not site_name:
                site_name = row['name']
            self.stdout.write('{title:30}'.format(**row))
        #Content.objects.create_folder(site_name, 'anything5')
        return site_name

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        site_name = self._sites()
        self._out(site_name)
        self._out(json.dumps(Content.objects.site_folders(site_name), indent=4))

        return
        file_name = options['file_name']
        self.stdout.write("Add '{}' to Alfresco".format(file_name))
        # upload a document to Alfresco
        self._upload_file(file_name)
        # update the properties on the document
        self._update_properties()
        self.stdout.write('{} is complete'.format(self.help))
