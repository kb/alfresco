# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-11 11:00
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('workflow', '0005_auto_20170823_1740'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alfresco', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentToCopy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('date_deleted', models.DateTimeField(blank=True, null=True)),
                ('file_to_copy', models.FileField(upload_to='')),
                ('completed_date', models.DateTimeField(blank=True, null=True)),
                ('retries', models.PositiveIntegerField(default=0)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('user_deleted', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('workflow', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workflow.Workflow')),
            ],
            options={
                'verbose_name': 'Document to copy',
                'verbose_name_plural': 'Documents to copy',
                'ordering': ('user__username', 'completed_date'),
            },
        ),
        migrations.RenameField(
            model_name='content',
            old_name='node_ref',
            new_name='node_id',
        ),
    ]
