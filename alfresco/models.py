# -*- encoding: utf-8 -*-
import logging

from django.conf import settings
from django.core.files import File
from django.db import models
from reversion import revisions as reversion

from alfresco.alfresco import Alfresco
from base.model_utils import TimedCreateModifyDeleteModel, TimeStampedModel
from workflow.models import Workflow


logger = logging.getLogger(__name__)


def _alfresco():
    return Alfresco(
        settings.ALFRESCO_URL,
        settings.ALFRESCO_USER,
        settings.ALFRESCO_PASS,
    )


class ContentManager(models.Manager):

    def create_content(self, node_id):
        obj = self.model(node_id=node_id)
        obj.save()
        return obj

    def upload(self, path_file, alfresco_node_id):
        """Upload to alfresco and create a 'Content' model."""
        alfresco = _alfresco()
        node_id = alfresco.upload(path_file, alfresco_node_id)
        self.create_content(node_id)


class Content(TimedCreateModifyDeleteModel):
    """Alfresco content.

    The core data structure in the Alfresco Repository is called a node. Each
    piece of content in the Repository has a corresponding node data structure
    to reference the content and metadata.
    Each node has a corresponding ``NodeRef`` uniquely identifying it in the
    Repository.

    - No need for reversion because we only add to the model?

    """

    node_id = models.TextField()
    objects = ContentManager()

    class Meta:
        ordering = ['-created']
        verbose_name = 'Alfresco Content'
        verbose_name_plural = 'Alfresco Content'

    def __str__(self):
        return '{}  {}'.format(
            self.created.strftime('%d/%m/%Y H:m'),
            self.node_id,
        )

    def node(self):
        alfresco = _alfresco()
        return alfresco.node(self.node_id)


class DocumentToCopyManager(models.Manager):

    def create_document_to_copy(self, user, workflow, file_path, file_name):
        to_copy = self.model(user=user, workflow=workflow)
        with open(file_path, 'rb') as f:
            django_file = File(f)
            to_copy.file_to_copy.save(file_name, django_file, save=True)
        return to_copy

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def outstanding(self):
        return self.model.objects.current().filter(
            completed_date__isnull=True,
            retries__lte=10,
        ).order_by(
            'created',
        )


class DocumentToCopy(TimedCreateModifyDeleteModel):
    """Documents to copy to Alfresco.

    - No need for reversion because we only add to the model?
    - We could add a JSON field for meta-data.

    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    workflow = models.ForeignKey(Workflow, on_delete=models.CASCADE)
    file_to_copy = models.FileField()
    completed_date = models.DateTimeField(blank=True, null=True)
    retries = models.PositiveIntegerField(default=0)
    objects = DocumentToCopyManager()

    class Meta:
        ordering = ('user__username', 'completed_date')
        verbose_name = 'Document to copy'
        verbose_name_plural = 'Documents to copy'

    def __str__(self):
        return "'{}' for '{}' workflow (user: '{}')".format(
            self.file_to_copy,
            self.workflow.name,
            self.user.username,
        )


class WorkflowFolderManager(models.Manager):

    def create_workflow_folder(self, workflow, alfresco_node_id):
        obj = self.model(workflow=workflow, alfresco_node_id=alfresco_node_id)
        obj.save()
        return obj

    def init_workflow_folder(self, workflow, alfresco_node_id):
        try:
            obj = self.model.objects.get(workflow=workflow)
            obj.alfresco_node_id = alfresco_node_id
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_workflow_folder(workflow, alfresco_node_id)
        return obj

    def node_folders_for_choice(self, node_id):
        result = []
        alfresco = _alfresco()
        data = alfresco.node_folders(node_id)
        for row in data:
            result.append((row.node_id, row.name))
        return result

    def nodes_for_choice_field(self):
        result = []
        alfresco = _alfresco()
        data = alfresco.sites()
        for node in data:
            result.append((node.node_id, node.title))
        return result


class WorkflowFolder(TimeStampedModel):
    """Each workflow has a folder inside Alfresco for storing documents."""

    workflow = models.OneToOneField(Workflow, on_delete=models.CASCADE)
    alfresco_node_id = models.TextField(
        help_text="Alfresco folder (node ID) for workflow"
    )
    objects = WorkflowFolderManager()

    class Meta:
        ordering = ['workflow__process_key']
        verbose_name = 'Workflow Folder'
        verbose_name_plural = 'Workflow Folders'

    def __str__(self):
        return '{}: {}'.format(
            self.workflow.process_key,
            self.alfresco_node_id,
        )


reversion.register(WorkflowFolder)
