# -*- encoding: utf-8 -*-
import logging

from django.db import DatabaseError, transaction
from django.utils import timezone

from alfresco.alfresco import AlfrescoError
from alfresco.models import (
    Content,
    DocumentToCopy,
    WorkflowFolder,
)


logger = logging.getLogger(__name__)


def _copy_to_alfresco(to_copy):
    result = False
    logger.info("Copy '{}' to Alfresco".format(to_copy.pk))
    try:
        workflow_folder = _workflow_folder(to_copy.workflow)
        # Content.objects.create_folder(
        #     workflow_folder.alfresco_node_id,
        #     'orange',
        # )
        Content.objects.upload(
            to_copy.file_to_copy.path,
            workflow_folder.alfresco_node_id,
        )
        # do we want to delete the file?
        to_copy.completed_date = timezone.now()
        logger.info("Copied '{}' to Alfresco".format(to_copy.pk))
        result = True
    except AlfrescoError as e:
        logger.exception(e)
        to_copy.retries += 1
        logger.info("Copy failed: Retry count for '{}' is {}".format(
            to_copy.pk,
            to_copy.retries,
        ))
    to_copy.save()
    return result


def _workflow_folder(workflow):
    """Try and find a workflow folder.

    Raise an ``AlfrescoError`` if the folder is missing (so we get the retry
    count incremented on the ``DocumentToCopy`` object).

    """
    try:
        return WorkflowFolder.objects.get(workflow=workflow)
    except WorkflowFolder.DoesNotExist:
        raise AlfrescoError(
            "Workflow: '{}' ('{}') does not have an Alfresco folder "
            "setup".format(workflow.name, workflow.process_key)
        )


def copy_to_alfresco(pk=None):
    result = []
    if pk is None:
        pks = [x.pk for x in DocumentToCopy.objects.outstanding()]
    else:
        pks = [pk]
    if not pks:
        logger.info("No files to copy to Alfresco")
    for pk in pks:
        with transaction.atomic():
            try:
                # only copy once (if function is called twice at the same time)
                to_copy = DocumentToCopy.objects.select_for_update(
                    nowait=True
                ).get(
                    pk=pk,
                )
                if not to_copy.completed_date:
                    if _copy_to_alfresco(to_copy):
                        result.append(to_copy.pk)
            except DatabaseError:
                # record is locked, so leave alone this time
                pass
    return result
