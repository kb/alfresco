# -*- encoding: utf-8 -*-
from celery import shared_task

from .service import copy_to_alfresco


@shared_task
def alfresco_copy(pk):
    return copy_to_alfresco(pk)


@shared_task
def alfresco_copy_all():
    return copy_to_alfresco()
