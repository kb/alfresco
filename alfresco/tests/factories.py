# -*- encoding: utf-8 -*-
import factory

from alfresco.models import Content, DocumentToCopy
from login.tests.factories import UserFactory
from workflow.tests.factories import WorkflowFactory


class ContentFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Content

    @factory.sequence
    def node_id(n):
        return 'node_id_{:02d}'.format(n)


class DocumentToCopyFactory(factory.django.DjangoModelFactory):

    file_to_copy = factory.django.FileField()
    workflow = factory.SubFactory(WorkflowFactory)

    class Meta:
        model = DocumentToCopy

    @factory.sequence
    def user(n):
        return UserFactory(username='user_{:02d}'.format(n))
