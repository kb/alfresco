# -*- encoding: utf-8 -*-
import os
import pytest

from httmock import HTTMock, response, urlmatch
from rest_framework import status

from alfresco.alfresco import Alfresco, AlfrescoError, Node, Site


@urlmatch(path=r'(.*)/my-node-id/children$')
def mock_response_children_created(url, request):
    content = {
        "entry": {
            "aspectNames": [
                "cm:auditable"
                ],
            "modifiedByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                },
            "modifiedAt": "2017-09-11T08:13:11.388+0000",
            "isFolder": True,
            "isFile": False,
            "name": "peach",
            "createdByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                },
            "parentId": "7937c287-fc26-4d58-8a91-46a70c261e4d",
            "createdAt": "2017-09-11T08:13:11.388+0000",
            "id": "a352d2cc-28ce-4b2e-8932-fbf6c1a5a250",
            "nodeType": "cm:folder"
            }
        }
    return response(status.HTTP_201_CREATED, content)


def mock_response_error(url, request):
    headers = {'content-type': 'application/json'}
    content = {
        'headers': {
            'content-type': 'apple',
        }
    }
    return response(status.HTTP_500_INTERNAL_SERVER_ERROR, content, headers)


@urlmatch(path=r'(.*)/sites/hatherleighinfo$')
def mock_response_sites_hatherleighinfo(url, request):
    content = {
        "tagScope": "/alfresco/service/api/tagscopes/workspace/SpacesStore/7937c287-fc26-4d58-8a91-46a70c261e4d",
        "title": "hatherleigh.info",
        "visibility": "MODERATED",
        "isPublic": False,
        "node": "/alfresco/service/api/node/workspace/SpacesStore/7937c287-fc26-4d58-8a91-46a70c261e4d",
        "url": "/alfresco/service/api/sites/hatherleighinfo",
        "siteManagers": [
            "patrick"
        ],
        "shortName": "hatherleighinfo",
        "sitePreset": "site-dashboard",
        "description": ""
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/sites$')
def mock_response_sites(url, request):
    content = [
        {
            "shortName": "hatherleighinfo",
            "url": "/alfresco/service/api/sites/hatherleighinfo",
            "isPublic": False,
            "isMemberOfGroup": False,
            "visibility": "MODERATED",
            "description": "",
            "tagScope": "/alfresco/service/api/tagscopes/workspace/SpacesStore/7937c287-fc26-4d58-8a91-46a70c261e4d",
            "title": "hatherleigh.info",
            "node": "/alfresco/service/api/node/workspace/SpacesStore/7937c287-fc26-4d58-8a91-46a70c261e4d",
            "sitePreset": "site-dashboard",
            "siteManagers": [
                "patrick"
            ]
        },
        {
            "shortName": "swsdp",
            "url": "/alfresco/service/api/sites/swsdp",
            "isPublic": True,
            "isMemberOfGroup": False,
            "visibility": "PUBLIC",
            "description": "This is a Sample Team site.",
            "tagScope": "/alfresco/service/api/tagscopes/workspace/SpacesStore/b4cff62a-664d-4d45-9302-98723eac1319",
            "title": "Sample: Web Site Design Project",
            "node": "/alfresco/service/api/node/workspace/SpacesStore/b4cff62a-664d-4d45-9302-98723eac1319",
            "sitePreset": "site-dashboard",
            "siteManagers": [
                "malcolm",
                "admin",
                "mjackson",
                "sam"
            ]
        }
    ]
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-node-id$')
def mock_response_node(url, request):
    content = {
        "entry": {
            "isFile": False,
            "isFolder": True,
            "createdAt": "2017-03-10T23:16:02.953+0000",
            "aspectNames": [
                "cm:tagscope",
                "cm:titled",
                "cm:auditable"
                ],
            "id": "7937c287-fc26-4d58-8a91-46a70c261e4d",
            "name": "hatherleighinfo",
            "modifiedAt": "2017-03-21T08:19:08.129+0000",
            "parentId": "c2c55db8-8557-4cee-af2b-28d4d1fd410b",
            "nodeType": "st:site",
            "createdByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                },
            "properties": {
                "cm:description": "Hatherleigh",
                "st:siteVisibility": "MODERATED",
                "cm:title": "hatherleigh.info",
                "st:sitePreset": "site-dashboard"
                },
            "modifiedByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                }
            }
        }
    return response(status.HTTP_200_OK, content)


def mock_response_node_folders(url, request):
    content = {
        "list": {
            "entries": [
                {
                    "entry": {
                        "nodeType": "cm:folder",
                        "parentId": "7937c287-fc26-4d58-8a91-46a70c261e4d",
                        "createdByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            },
                        "id": "98c438f6-5cbe-4697-8a01-58efe078bb92",
                        "name": "documentLibrary",
                        "isFile": False,
                        "modifiedAt": "2017-05-19T14:45:31.689+0000",
                        "createdAt": "2017-03-10T23:16:12.849+0000",
                        "isFolder": True,
                        "modifiedByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            }
                        }
                    },
                {
                    "entry": {
                        "nodeType": "cm:folder",
                        "parentId": "7937c287-fc26-4d58-8a91-46a70c261e4d",
                        "createdByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            },
                        "id": "a352d2cc-28ce-4b2e-8932-fbf6c1a5a250",
                        "name": "peach",
                        "isFile": False,
                        "modifiedAt": "2017-09-11T08:13:11.388+0000",
                        "createdAt": "2017-09-11T08:13:11.388+0000",
                        "isFolder": True,
                        "modifiedByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            }
                        }
                    }
                ],
            "pagination": {
                "totalItems": 2,
                "hasMoreItems": False,
                "maxItems": 100,
                "count": 2,
                "skipCount": 0
                }
            }
        }
    return response(status.HTTP_200_OK, content)


def mock_response_node_type(url, request):
    content = {
        "entry": {
            "properties": {
                "exif:pixelXDimension": 960,
                "cm:versionLabel": "1.0",
                "fn:invoiceNumber": "987654",
                "cm:versionType": "MAJOR",
                "cm:lastThumbnailModification": [
                    "imgpreview:1504542484853"
                    ],
                "exif:pixelYDimension": 960
                },
            "aspectNames": [
                "rn:renditioned",
                "cm:versionable",
                "cm:titled",
                "cm:auditable",
                "cm:author",
                "cm:thumbnailModification",
                "exif:exif"
                ],
            "parentId": "70c2edfd-341a-40a7-abfe-e3177f29b9a4",
            "createdAt": "2017-09-04T16:27:38.994+0000",
            "modifiedAt": "2017-09-11T09:26:24.597+0000",
            "createdByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                },
            "name": "21272662_10155018851002992_3634713870940413425_n.jpg",
            "modifiedByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                },
            "id": "2fefcd5b-25a2-4100-acb3-c42edb0e085e",
            "content": {
                "encoding": "UTF-8",
                "sizeInBytes": 87038,
                "mimeType": "image/jpeg",
                "mimeTypeName": "JPEG Image"
                },
            "nodeType": "fn:Purchase_Invoices",
            "isFile": True,
            "isFolder": False
        }
    }
    return response(status.HTTP_200_OK, content)


def mock_response_upload(url, request):
    headers = {'content-type': 'application/json'}
    content = {
        "entry": {
            "content": {
                "mimeType": "image/jpeg",
                "mimeTypeName": "JPEG Image",
                "encoding": "UTF-8",
                "sizeInBytes": 18779
            },
            "parentId": "70c2edfd-341a-40a7-abfe-e3177f29b9a4",
            "name": "profile_If2TtFl.jpg",
            "properties": {
                "exif:pixelYDimension": 403,
                "cm:versionLabel": "1.0",
                "exif:pixelXDimension": 403,
                "cm:versionType": "MAJOR"
            },
            "modifiedAt": "2017-05-03T07:00:27.669+0000",
            "id": "1d9122c6-3c6f-4740-a272-6a90d0a50a49",
            "createdAt": "2017-05-03T07:00:27.669+0000",
            "nodeType": "cm:content",
            "modifiedByUser": {
                "id": "patrick",
                "displayName": "Patrick Kimber"
            },
            "isFile": True,
            "isFolder": False,
            "createdByUser": {
                "id": "patrick",
                "displayName": "Patrick Kimber"
            },
            "aspectNames": [
                "cm:versionable",
                "cm:titled",
                "cm:auditable",
                "cm:author",
                "exif:exif"
            ]
        }
    }
    return response(status.HTTP_201_CREATED, content, headers)


def mock_response_upload_error(url, request):
    headers = {'content-type': 'application/json'}
    content = {
        'content': 'Oh hai'
    }
    return response(status.HTTP_400_BAD_REQUEST, content, headers)


def test_create_folder():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_children_created):
        result = alfresco.create_folder('my-node-id', 'peach')
    assert 'a352d2cc-28ce-4b2e-8932-fbf6c1a5a250' == result


def test_http_get_error():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_error):
        with pytest.raises(AlfrescoError) as e:
            alfresco._http_get('http://orange')
    assert 'Cannot GET: http://orange' in str(e.value)


def test_http_post_error():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_error):
        with pytest.raises(AlfrescoError) as e:
            alfresco._http_post('http://orange', {'abc': 'def'})
    assert 'Cannot POST: http://orange' in str(e.value)


def test_http_put_error():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_error):
        with pytest.raises(AlfrescoError) as e:
            alfresco._http_put('http://orange', {'abc': 'def'})
    assert 'Cannot PUT: http://orange' in str(e.value)


def test_node():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_node):
        assert Node(
            node_id='7937c287-fc26-4d58-8a91-46a70c261e4d',
            name='hatherleighinfo',
            description='Hatherleigh',
            is_site=False,
            parent_id="c2c55db8-8557-4cee-af2b-28d4d1fd410b",
        ) == alfresco.node('my-node-id')


def test_node_folders():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_node_folders):
        assert [
            Node(
                node_id='98c438f6-5cbe-4697-8a01-58efe078bb92',
                name='documentLibrary',
                description=None,
                is_site=False,
                parent_id='7937c287-fc26-4d58-8a91-46a70c261e4d',
            ),
            Node(
                node_id="a352d2cc-28ce-4b2e-8932-fbf6c1a5a250",
                name='peach',
                description=None,
                is_site=False,
                parent_id="7937c287-fc26-4d58-8a91-46a70c261e4d",
            ),
        ] == alfresco.node_folders('7937c287-fc26-4d58-8a91-46a70c261e4d')


def test_service_url(settings):
    alfresco = Alfresco('http://localhost', 'test_user', 'test_pass')
    assert 'http://localhost' == alfresco._service_url


def test_set_node_type():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    properties = {
        'fn:invoiceNumber': '987654',
    }
    with HTTMock(mock_response_node_type):
        alfresco.set_node_type(
            '2fefcd5b-25a2-4100-acb3-c42edb0e085e',
            'fn:Purchase_Invoices',
            properties,
        )


def test_sites():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_sites):
        assert [
            Site(
                node_id='7937c287-fc26-4d58-8a91-46a70c261e4d',
                name='hatherleighinfo',
                title='hatherleigh.info',
            ),
            Site(
                node_id='b4cff62a-664d-4d45-9302-98723eac1319',
                name='swsdp',
                title='Sample: Web Site Design Project',
            ),
        ] == alfresco.sites()


def test_site_node_id():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    node_url = (
        '/alfresco/service/api/node/workspace/SpacesStore/'
        '7937c287-fc26-4d58-8a91-46a70c261e4d'
    )
    assert (
        '7937c287-fc26-4d58-8a91-46a70c261e4d'
    ) == alfresco._site_node_id(node_url)


def test_site_node_id_not_url():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    node_url = 'apple-orange'
    with pytest.raises(AlfrescoError) as e:
        alfresco._site_node_id(node_url)
    assert "Cannot find node in URL: 'apple-orange'" in str(e.value)


def test_site_node_url():
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_sites_hatherleighinfo):
        assert (
            '/alfresco/service/api/node/workspace/SpacesStore/'
            '7937c287-fc26-4d58-8a91-46a70c261e4d'
        ) == alfresco.site_node_url('hatherleighinfo')


def test_upload(settings):
    test_path_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'data',
        'test_file.txt'
    )
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_upload):
        result = alfresco.upload(test_path_file, 'abc')
    assert '1d9122c6-3c6f-4740-a272-6a90d0a50a49' == result


def test_upload_error(settings):
    test_path_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'data',
        'test_file.txt'
    )
    alfresco = Alfresco('http://localhost:1234', 'test_user', 'test_pass')
    with HTTMock(mock_response_upload_error):
        with pytest.raises(AlfrescoError) as e:
            alfresco.upload(test_path_file, 'xyz')
        assert 'Cannot upload file to Alfresco' in str(e.value)
        assert 'data/test_file.txt' in str(e.value)


def test_url_a(settings):
    alfresco = Alfresco('http://orange/', 'test_user', 'test_pass')
    assert (
        'http://orange/alfresco/api/-default-/public/alfresco/versions/1/'
        'nodes'
    ) == alfresco._url_a()


def test_url_a_path(settings):
    alfresco = Alfresco('http://orange/', 'test_user', 'test_pass')
    assert (
        'http://orange/alfresco/api/-default-/public/alfresco/versions/1/'
        'nodes/apple'
    ) == alfresco._url_a('apple')


def test_url_a_path_kwargs(settings):
    alfresco = Alfresco('http://orange/', 'test_user', 'test_pass')
    assert (
        'http://orange/alfresco/api/-default-/public/alfresco/versions/1/'
        'nodes/apple?colour=Green&fruit=True'
    ) == alfresco._url_a('apple', fruit=True, colour='Green')


def test_url_b(settings):
    alfresco = Alfresco('http://orange/', 'test_user', 'test_pass')
    assert 'http://orange/alfresco/service/api' == alfresco._url_b()


def test_url_b_path(settings):
    alfresco = Alfresco('http://orange/', 'test_user', 'test_pass')
    assert (
        'http://orange/alfresco/service/api/apple'
    ) == alfresco._url_b('apple')


def test_url_b_path_kwargs(settings):
    alfresco = Alfresco('http://orange/', 'test_user', 'test_pass')
    assert (
        'http://orange/alfresco/service/api/apple?colour=Green&fruit=True'
    ) == alfresco._url_b('apple', fruit=True, colour='Green')
