# -*- encoding: utf-8 -*-
import os
import pytest

from httmock import HTTMock, response
from rest_framework import status

from alfresco.models import Content
from alfresco.tests.factories import ContentFactory


def response_content(url, request):
    headers = {'content-type': 'application/json'}
    content = {
        'entry': {
            'id': '123',
        }
    }
    return response(status.HTTP_201_CREATED, content, headers)


@pytest.mark.django_db
def test_str():
    content = ContentFactory(node_id='123')
    assert '123' in str(content)


@pytest.mark.django_db
def test_upload(settings):
    test_path_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'data',
        'test_file.txt'
    )
    settings.ALFRESCO_PASS = 'password'
    settings.ALFRESCO_URL = 'http://orange/'
    settings.ALFRESCO_USER = 'patrick'
    with HTTMock(response_content):
        Content.objects.upload(test_path_file, 'abc')
    assert 1 == Content.objects.count()
