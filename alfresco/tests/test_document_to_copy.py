# -*- encoding: utf-8 -*-
import factory
import os
import pytest

from django.utils import timezone

from alfresco.models import DocumentToCopy
from alfresco.tests.factories import DocumentToCopyFactory
from login.tests.factories import UserFactory
from workflow.tests.factories import WorkflowFactory


@pytest.mark.django_db
def test_create_document_to_copy():
    user = UserFactory()
    test_path_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'data',
        'test_file.txt'
    )
    workflow = WorkflowFactory()
    to_copy = DocumentToCopy.objects.create_document_to_copy(
        user,
        workflow,
        test_path_file,
        'apple.txt',
    )
    to_copy.refresh_from_db()
    assert user == to_copy.user
    assert workflow == to_copy.workflow
    assert 'apple' in to_copy.file_to_copy.name
    assert b'xyz\n' == to_copy.file_to_copy.read()


@pytest.mark.django_db
def test_current():
    user = UserFactory()
    DocumentToCopyFactory(user=UserFactory(username='a'))
    DocumentToCopyFactory(user=UserFactory(username='b')).set_deleted(user)
    DocumentToCopyFactory(user=UserFactory(username='c'))
    assert [
        'a',
        'c',
    ] == [x.user.username for x in DocumentToCopy.objects.current()]


@pytest.mark.django_db
def test_outstanding():
    user = UserFactory()
    DocumentToCopyFactory(user=UserFactory(username='a'))
    DocumentToCopyFactory(user=UserFactory(username='b')).set_deleted(user)
    DocumentToCopyFactory(user=UserFactory(username='c'), retries=10)
    DocumentToCopyFactory(user=UserFactory(username='d'), retries=11)
    DocumentToCopyFactory(
        user=UserFactory(username='e'),
        completed_date=timezone.now(),
    )
    DocumentToCopyFactory(user=UserFactory(username='f'))
    assert [
        'a',
        'c',
        'f',
    ] == [x.user.username for x in DocumentToCopy.objects.outstanding()]


@pytest.mark.django_db
def test_str():
    """Test the ``str`` method.

    e.g. ``'document_y3tYGAV.txt' for 'validate' workflow 'pat'``

    """
    to_copy = DocumentToCopyFactory(
        file_to_copy=factory.django.FileField(filename='document.txt'),
        user=UserFactory(username='pat'),
        workflow=WorkflowFactory(name='validate'),
    )
    assert "for 'validate' workflow (user: 'pat')" in str(to_copy)
    assert '.txt' in str(to_copy)
    assert 'document' in str(to_copy)
