# -*- encoding: utf-8 -*-
import pytest

from alfresco.alfresco import AlfrescoError
from alfresco.models import WorkflowFolder
from alfresco.service import _workflow_folder
from workflow.tests.factories import WorkflowFactory


@pytest.mark.django_db
def test_workflow_folder():
    workflow = WorkflowFactory()
    folder = WorkflowFolder.objects.create_workflow_folder(workflow, 'xyz')
    assert folder == _workflow_folder(workflow)


@pytest.mark.django_db
def test_workflow_folder_missing():
    workflow = WorkflowFactory(name='Orange', process_key='apple')
    with pytest.raises(AlfrescoError) as e:
        _workflow_folder(workflow)
    assert (
        "Workflow: 'Orange' ('apple') does not have an Alfresco folder setup"
    ) in str(e.value)
