# -*- encoding: utf-8 -*-
import pytest

from alfresco.tasks import alfresco_copy, alfresco_copy_all
from alfresco.tests.factories import DocumentToCopyFactory


@pytest.mark.django_db
def test_alfresco_copy():
    to_copy = DocumentToCopyFactory()
    alfresco_copy(to_copy.pk)


@pytest.mark.django_db
def test_alfresco_copy_all():
    alfresco_copy_all()
