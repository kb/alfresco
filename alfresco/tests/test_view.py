# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from httmock import HTTMock, response, urlmatch
from rest_framework import status

from alfresco.models import WorkflowFolder
from alfresco.tests.factories import ContentFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from workflow.tests.factories import WorkflowFactory


def mock_response_content(url, request):
    return response(status.HTTP_200_OK, {})


def mock_response_node(url, request):
    content = {
        "entry": {
            "isFile": False,
            "isFolder": True,
            "createdAt": "2017-03-10T23:16:02.953+0000",
            "aspectNames": [
                "cm:tagscope",
                "cm:titled",
                "cm:auditable"
                ],
            "id": "7937c287-fc26-4d58-8a91-46a70c261e4d",
            "name": "hatherleighinfo",
            "modifiedAt": "2017-03-21T08:19:08.129+0000",
            "parentId": "c2c55db8-8557-4cee-af2b-28d4d1fd410b",
            "nodeType": "st:site",
            "createdByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                },
            "properties": {
                "cm:description": "Hatherleigh",
                "st:siteVisibility": "MODERATED",
                "cm:title": "hatherleigh.info",
                "st:sitePreset": "site-dashboard"
                },
            "modifiedByUser": {
                "displayName": "Patrick Kimber",
                "id": "patrick"
                }
            }
        }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-node-id/children$')
def mock_response_wizard_children(url, request):
    content = {
        'list': {
            'entries': [],
        }
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-node-id$')
def mock_response_wizard_node(url, request):
    content = {
        'entry': {
            'id': 'my-node-id',
            'name': 'My Node',
            'nodeType': 'st:sites',
            'parentId': 'my-parent-id',
        }
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-parent-id$')
def mock_response_wizard_parent(url, request):
    content = {
        'entry': {
            'id': 'my-node-id',
            'name': 'My Node',
            'nodeType': 'st:sites',
            'parentId': 'my-parent-id',
        }
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/sites$')
def mock_response_wizard_sites(url, request):
    content = {
    }
    return response(status.HTTP_200_OK, content)


@pytest.mark.django_db
def test_content_list(client):
    ContentFactory()
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    with HTTMock(mock_response_content):
        response = client.get(reverse('alfresco.content.list'))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_folder_list(client):
    workflow = WorkflowFactory()
    WorkflowFolder.objects.create_workflow_folder(workflow, 'my-node-id')
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    with HTTMock(mock_response_node):
        response = client.get(reverse('alfresco.folder.list'))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_folder_wizard_site_folder(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    workflow = WorkflowFactory()
    WorkflowFolder.objects.create_workflow_folder(workflow, 'my-node-id')
    url = reverse('alfresco.folder.wizard', args=[workflow.pk, 'my-node-id'])
    with HTTMock(
            mock_response_wizard_children
        ), HTTMock(
            mock_response_wizard_node
        ), HTTMock(
            mock_response_wizard_parent
        ), HTTMock(
            mock_response_wizard_sites
        ):
        response = client.get(url)
