# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from httmock import HTTMock, response, urlmatch
from rest_framework import status

from alfresco.tests.factories import ContentFactory, DocumentToCopyFactory
from login.tests.fixture import perm_check
from workflow.tests.factories import WorkflowFactory


def mock_response_node(url, request):
    content = [
        {
            'node': 'http://localhost:1234/apple',
            'shortName': 'apple',
            'title': 'Orange',
        },
    ]
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-node-id/children$')
def mock_response_wizard_children(url, request):
    content = {
        'list': {
            'entries': [],
        }
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-node-id$')
def mock_response_wizard_node(url, request):
    content = {
        'entry': {
            'id': 'my-node-id',
            'name': 'My Node',
            'nodeType': 'st:sites',
            'parentId': 'my-parent-id',
        }
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/my-parent-id$')
def mock_response_wizard_parent(url, request):
    content = {
        'entry': {
            'id': 'my-node-id',
            'name': 'My Node',
            'nodeType': 'st:sites',
            'parentId': 'my-parent-id',
        }
    }
    return response(status.HTTP_200_OK, content)


@urlmatch(path=r'(.*)/sites$')
def mock_response_wizard_sites(url, request):
    content = {
    }
    return response(status.HTTP_200_OK, content)


@pytest.mark.django_db
def test_detail(perm_check):
    content = ContentFactory(node_id='my-node-id')
    url = reverse('alfresco.content.detail', args=[content.pk])
    with HTTMock(mock_response_wizard_node):
        perm_check.admin(url)


@pytest.mark.django_db
def test_document_to_copy_list(perm_check):
    DocumentToCopyFactory()
    perm_check.admin(reverse('alfresco.document.to.copy.list'))


@pytest.mark.django_db
def test_folder_list(perm_check):
    WorkflowFactory()
    url = reverse('alfresco.folder.list')
    perm_check.admin(url)


@pytest.mark.django_db
def test_folder_wizard_site(perm_check):
    workflow = WorkflowFactory()
    url = reverse('alfresco.folder.wizard', args=[workflow.pk])
    with HTTMock(mock_response_node):
        perm_check.admin(url)


@pytest.mark.django_db
def test_folder_wizard_site_folder(perm_check):
    workflow = WorkflowFactory()
    url = reverse('alfresco.folder.wizard', args=[workflow.pk, 'my-node-id'])
    with HTTMock(
            mock_response_wizard_children
        ), HTTMock(
            mock_response_wizard_node
        ), HTTMock(
            mock_response_wizard_parent
        ), HTTMock(
            mock_response_wizard_sites
        ):
        perm_check.admin(url)


@pytest.mark.django_db
def test_list(perm_check):
    url = reverse('alfresco.content.list')
    perm_check.admin(url)
