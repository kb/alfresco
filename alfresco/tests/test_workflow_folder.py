# -*- encoding: utf-8 -*-
import pytest

from httmock import HTTMock, response
from rest_framework import status

from alfresco.models import WorkflowFolder
from workflow.tests.factories import WorkflowFactory


def mock_response_node_folders(url, request):
    content = {
        "list": {
            "entries": [
                {
                    "entry": {
                        "nodeType": "cm:folder",
                        "parentId": "7937c287-fc26-4d58-8a91-46a70c261e4d",
                        "createdByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            },
                        "id": "98c438f6-5cbe-4697-8a01-58efe078bb92",
                        "name": "documentLibrary",
                        "isFile": False,
                        "modifiedAt": "2017-05-19T14:45:31.689+0000",
                        "createdAt": "2017-03-10T23:16:12.849+0000",
                        "isFolder": True,
                        "modifiedByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            }
                        }
                    },
                {
                    "entry": {
                        "nodeType": "cm:folder",
                        "parentId": "7937c287-fc26-4d58-8a91-46a70c261e4d",
                        "createdByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            },
                        "id": "a352d2cc-28ce-4b2e-8932-fbf6c1a5a250",
                        "name": "peach",
                        "isFile": False,
                        "modifiedAt": "2017-09-11T08:13:11.388+0000",
                        "createdAt": "2017-09-11T08:13:11.388+0000",
                        "isFolder": True,
                        "modifiedByUser": {
                            "id": "patrick",
                            "displayName": "Patrick Kimber"
                            }
                        }
                    }
                ],
            "pagination": {
                "totalItems": 2,
                "hasMoreItems": False,
                "maxItems": 100,
                "count": 2,
                "skipCount": 0
                }
            }
        }
    return response(status.HTTP_200_OK, content)


def mock_response_sites(url, request):
    content = [
        {
            "shortName": "hatherleighinfo",
            "url": "/alfresco/service/api/sites/hatherleighinfo",
            "isPublic": False,
            "isMemberOfGroup": False,
            "visibility": "MODERATED",
            "description": "",
            "tagScope": "/alfresco/service/api/tagscopes/workspace/SpacesStore/7937c287-fc26-4d58-8a91-46a70c261e4d",
            "title": "hatherleigh.info",
            "node": "/alfresco/service/api/node/workspace/SpacesStore/7937c287-fc26-4d58-8a91-46a70c261e4d",
            "sitePreset": "site-dashboard",
            "siteManagers": [
                "patrick"
            ]
        },
        {
            "shortName": "swsdp",
            "url": "/alfresco/service/api/sites/swsdp",
            "isPublic": True,
            "isMemberOfGroup": False,
            "visibility": "PUBLIC",
            "description": "This is a Sample Team site.",
            "tagScope": "/alfresco/service/api/tagscopes/workspace/SpacesStore/b4cff62a-664d-4d45-9302-98723eac1319",
            "title": "Sample: Web Site",
            "node": "/alfresco/service/api/node/workspace/SpacesStore/b4cff62a-664d-4d45-9302-98723eac1319",
            "sitePreset": "site-dashboard",
            "siteManagers": [
                "malcolm",
                "admin",
                "mjackson",
                "sam"
            ]
        }
    ]
    return response(status.HTTP_200_OK, content)


@pytest.mark.django_db
def test_create():
    workflow_folder = WorkflowFolder.objects.create_workflow_folder(
        workflow=WorkflowFactory(process_key='orange'),
        alfresco_node_id='123',
    )
    assert 'orange: 123' == str(workflow_folder)


@pytest.mark.django_db
def test_init():
    workflow_folder = WorkflowFolder.objects.init_workflow_folder(
        workflow=WorkflowFactory(process_key='pear'),
        alfresco_node_id='543',
    )
    assert 'pear: 543' == str(workflow_folder)


def test_node_folders_for_choice():
    with HTTMock(mock_response_node_folders):
        assert [
            ('98c438f6-5cbe-4697-8a01-58efe078bb92', 'documentLibrary'),
            ('a352d2cc-28ce-4b2e-8932-fbf6c1a5a250', 'peach')
        ] == WorkflowFolder.objects.node_folders_for_choice('my-node-id')


def test_nodes_for_choice_field():
    with HTTMock(mock_response_sites):
        assert [
            ('7937c287-fc26-4d58-8a91-46a70c261e4d', 'hatherleigh.info'),
            ('b4cff62a-664d-4d45-9302-98723eac1319', 'Sample: Web Site'),
        ] == WorkflowFolder.objects.nodes_for_choice_field()


@pytest.mark.django_db
def test_ordering():
    WorkflowFolder(
        workflow=WorkflowFactory(process_key='orange'),
        alfresco_node_id='123',
    )
    WorkflowFolder(
        workflow=WorkflowFactory(process_key='apple'),
        alfresco_node_id='456',
    )
    WorkflowFolder.objects.all()


@pytest.mark.django_db
def test_str():
    workflow_folder = WorkflowFolder(
        workflow=WorkflowFactory(process_key='apple'),
        alfresco_node_id='A123',
    )
    assert 'apple: A123' == str(workflow_folder)
