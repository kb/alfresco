# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import (
    ContentDetailView,
    ContentListView,
    DocumentToCopyListView,
    FolderListView,
    FolderWizardFolderFormView,
    FolderWizardNodeFormView,
)


urlpatterns = [
    url(regex=r'^content/$',
        view=ContentListView.as_view(),
        name='alfresco.content.list'
        ),
    url(regex=r'^content/(?P<pk>\d+)/$',
        view=ContentDetailView.as_view(),
        name='alfresco.content.detail'
        ),
    url(regex=r'^document/to/copy/$',
        view=DocumentToCopyListView.as_view(),
        name='alfresco.document.to.copy.list'
        ),
    url(regex=r'^folder/$',
        view=FolderListView.as_view(),
        name='alfresco.folder.list'
        ),
    url(regex=r'^folder/(?P<workflow_pk>\d+)/wizard/$',
        view=FolderWizardNodeFormView.as_view(),
        name='alfresco.folder.wizard'
        ),
    url(regex=r'^folder/(?P<workflow_pk>\d+)/wizard/(?P<node>[-\w]+)/$',
        view=FolderWizardFolderFormView.as_view(),
        name='alfresco.folder.wizard'
        ),
]
