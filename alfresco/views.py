# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from django.conf import settings
from django.urls import reverse
from django.views.generic import DetailView, FormView, ListView

from alfresco.alfresco import Alfresco
from alfresco.forms import FolderWizardFolderForm, FolderWizardNodeForm
from alfresco.models import Content, DocumentToCopy, WorkflowFolder
from base.view_utils import BaseMixin
from workflow.models import Workflow


class ContentDetailView(
        LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, DetailView):

    model = Content


class ContentListView(
        LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView):

    model = Content
    paginate_by = 20


class DocumentToCopyListView(
        LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView):

    model = DocumentToCopy
    paginate_by = 20


class FolderListView(
        LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView):

    template_name = 'alfresco/folder_list.html'

    def _alfresco(self):
        return Alfresco(
            settings.ALFRESCO_URL,
            settings.ALFRESCO_USER,
            settings.ALFRESCO_PASS,
        )

    def get_queryset(self):
        result = []
        alfresco = self._alfresco()
        qs = Workflow.objects.current()
        for row in qs:
            node = None
            try:
                workflow_folder = WorkflowFolder.objects.get(workflow=row)
                node_id = workflow_folder.alfresco_node_id
                node = alfresco.node(node_id)
            except WorkflowFolder.DoesNotExist:
                pass
            result.append({
                'pk': row.pk,
                'name': row.name,
                'node': node,
            })
        return result


class FolderWizardMixin:

    template_name = 'alfresco/folder_wizard_form.html'

    def _alfresco(self):
        return Alfresco(
            settings.ALFRESCO_URL,
            settings.ALFRESCO_USER,
            settings.ALFRESCO_PASS,
        )

    def _node(self, node_id):
        alfresco = self._alfresco()
        return alfresco.node(node_id)

    def _workflow(self):
        return Workflow.objects.get(pk=self._workflow_pk())

    def _workflow_folder(self):
        try:
            result = WorkflowFolder.objects.get(
                workflow__pk=self._workflow_pk(),
            )
        except WorkflowFolder.DoesNotExist:
            result = None
        return result

    def _workflow_node(self):
        result = None
        workflow_folder = self._workflow_folder()
        if workflow_folder:
            alfresco = self._alfresco()
            result = alfresco.node(workflow_folder.alfresco_node_id)
        return result

    def _workflow_pk(self):
        return self.kwargs['workflow_pk']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(
            alfresco_site=settings.ALFRESCO_URL,
            workflow_node=self._workflow_node(),
            workflow=self._workflow(),
        ))
        return context


class FolderWizardFolderFormView(
        LoginRequiredMixin, SuperuserRequiredMixin,
        FolderWizardMixin, BaseMixin, FormView):

    form_class = FolderWizardFolderForm

    def _alfresco(self):
        return Alfresco(
            settings.ALFRESCO_URL,
            settings.ALFRESCO_USER,
            settings.ALFRESCO_PASS,
        )

    def _back_to(self, node):
        print('Parent -------------------------------------------------------')
        alfresco = self._alfresco()
        parent = alfresco.node(node.parent_id)
        if parent.is_site:
            result = reverse(
                'alfresco.folder.wizard',
                args=[self._workflow_pk()]
            )
        else:
            result = reverse(
                'alfresco.folder.wizard',
                args=[self._workflow_pk(), parent.node_id]
            )
        return result

    def _node_id(self):
        """The Alfresco node."""
        node_id = self.kwargs['node']
        return node_id

    def form_valid(self, form):
        """Save the site so we can use it in ``get_success_url``."""
        self.selected = False
        self.alfresco_folder = form.cleaned_data.get('folder')
        if 'select' in self.request.POST:
            workflow = Workflow.objects.get(pk=self._workflow_pk())
            WorkflowFolder.objects.init_workflow_folder(
                workflow,
                self.alfresco_folder,
            )
            self.selected = True
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        node_id = self._node_id()
        node = self._node(node_id)
        context.update(dict(
            current_node=node,
            back_to=self._back_to(node),
        ))
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(node_id=self._node_id()))
        return result

    def get_success_url(self):
        if self.selected:
            return reverse('alfresco.folder.list')
        else:
            return reverse(
                'alfresco.folder.wizard',
                args=[self._workflow_pk(), self.alfresco_folder]
            )


class FolderWizardNodeFormView(
        LoginRequiredMixin, SuperuserRequiredMixin,
        FolderWizardMixin, BaseMixin, FormView):

    form_class = FolderWizardNodeForm

    def form_valid(self, form):
        """Save the site so we can use it in ``get_success_url``."""
        self.alfresco_node = form.cleaned_data.get('node')
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(
            alfresco_node=None,
            back_to=reverse('alfresco.folder.list'),
        ))
        return context

    def get_success_url(self):
        return reverse(
            'alfresco.folder.wizard',
            args=[self._workflow_pk(), self.alfresco_node]
        )
