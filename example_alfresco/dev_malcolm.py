# -*- encoding: utf-8 -*-

from __future__ import unicode_literals
from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'temp.db',                               # Or path to database file if using sqlite3.
        'USER': '',                               # Not used with sqlite3.
        'PASSWORD': '',                           # Not used with sqlite3.
        'HOST': '',                              # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                                          # Set to empty string for default. Not used with sqlite3.
    }
}

if DEBUG:
    ALLOW_DEBUG_TOOLBAR=True
    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

    INSTALLED_APPS += (
        # 'django.contrib.formtools',
        'debug_toolbar',
    )

    #force the debug toolbar to be displayed
    def show_toolbar(request):
        return True

    DEBUG_TOOLBAR_CONFIG = {
        "SHOW_TOOLBAR_CALLBACK" : show_toolbar,
    }


    import socket

    ALLOWED_HOSTS = [
        '127.0.0.1',
        '127.0.1.1',
        'localhost',
        socket.gethostbyname(socket.gethostname())
    ]
