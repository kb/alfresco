# -*- encoding: utf-8 -*-
from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'temp.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

# INSTALLED_APPS += (
#     'debug_toolbar',
#     'debug_panel',
# )
#
# MIDDLEWARE_CLASSES += (
#     'debug_panel.middleware.DebugPanelMiddleware',
# )
#
# def show_toolbar(request):
#     return DEBUG
#
# DEBUG_TOOLBAR_CONFIG = {
#     'SHOW_TOOLBAR_CALLBACK': lambda r: DEBUG
# }
