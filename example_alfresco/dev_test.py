# -*- encoding: utf-8 -*-
from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dev_test_record',
        'USER': get_env_variable('DATABASE_USER'),
        'PASSWORD': '',
        'HOST': get_env_variable('DATABASE_HOST'),
        'PORT': '',
    }
}
