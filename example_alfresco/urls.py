# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import DashView, HomeView, SettingsView


admin.autodiscover()


urlpatterns = [
    url(regex=r'^$',
        view=HomeView.as_view(),
        name='project.home'
        ),
    url(regex=r'^dash/$',
        view=DashView.as_view(),
        name='project.dash'
        ),
    url(regex=r'^settings/$',
        view=SettingsView.as_view(),
        name='project.settings'
        ),
    url(regex=r'^',
        view=include('login.urls')
        ),
    url(regex=r'^admin/',
        view=admin.site.urls
        ),
    url(regex=r'^alfresco/',
        view=include('alfresco.urls')
        ),
    url(regex=r'^workflow/',
        view=include('workflow.urls')
        ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.ALLOW_DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls)), ]
